set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_PROCESSOR arm)

# set(CMAKE_SYSROOT /home/devel/rasp-pi-rootfs)
# set(CMAKE_STAGING_PREFIX /home/devel/stage)

if (WIN32)
    # set(TOOLCHAIN_PATH C:/gcc-arm-none-eabi-9-2020-q2-update-win32)
    set(TOOLCHAIN_PREFIX arm-none-eabi-)

    set(CMAKE_ASM_COMPILER ${TOOLCHAIN_PREFIX}gcc.exe)
    set(CMAKE_C_COMPILER ${TOOLCHAIN_PREFIX}gcc.exe)
    set(CMAKE_CXX_COMPILER ${TOOLCHAIN_PREFIX}gcc.exe)
    set(CMAKE_AR ${TOOLCHAIN_PREFIX}ar.exe)
    set(CMAKE_OBJCOPY ${TOOLCHAIN_PREFIX}objcopy.exe)
    set(CMAKE_OBJDUMP ${TOOLCHAIN_PREFIX}objdump.exe)
    set(CMAKE_SIZE ${TOOLCHAIN_PREFIX}size.exe)

elseif(UNIX)

    set(CMAKE_ASM_COMPILER arm-none-eabi-gcc)
    set(CMAKE_C_COMPILER arm-none-eabi-gcc)
    set(CMAKE_CXX_COMPILER arm-none-eabi-g++)
    set(CMAKE_AR arm-none-eabi-ar)
    set(CMAKE_OBJCOPY arm-none-eabi-objcopy)
    set(CMAKE_OBJDUMP arm-none-eabi-objdump)
    set(CMAKE_SIZE arm-none-eabi-objdump)

endif()


# find additional toolchain executables
find_program(ARM_SIZE_EXECUTABLE arm-none-eabi-size)
find_program(ARM_GDB_EXECUTABLE arm-none-eabi-gdb)
find_program(ARM_OBJCOPY_EXECUTABLE arm-none-eabi-objcopy)
find_program(ARM_OBJDUMP_EXECUTABLE arm-none-eabi-objdump)

#set(CMAKE_TRY_COMPILE_TARGET_TYPE STATIC_LIBRARY)

# search for program/library/include in the build host directories
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)