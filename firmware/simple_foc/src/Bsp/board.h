/**
 * @file bsp.h
 * @author jimingqing
 * @date 2023-07-14
 * @version 0.0.1
 * @copyright XiazhiTech Copyright (c) 2023
 *
 * @brief
 */

#ifndef __BOARD_H__
#define __BOARD_H__

#include "stm32f1xx_hal.h"

#include <rthw.h>
#include <rtthread.h>

#include "usart.h"
#include "gpio.h"

#define USART2_EN_Pin GPIO_PIN_4
#define USART2_EN_GPIO_Port GPIOA
#define DRV_EN_Pin GPIO_PIN_15
#define DRV_EN_GPIO_Port GPIOB

extern void Error_Handler(void);

#endif /* __BOARD_H__ */
